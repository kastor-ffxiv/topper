# TOPPER

Topper is a simple container application to keep a window always on top of others.

Topper is currently built for Windows, with OSX and Linux binaries coming soon. 
Feel free to download the source and package it for use yourself - but OS-specific 
considerations have not yet been implemented.

## Features
- Topper can display images, videos, HTML/XML documents and web pages.
- Incremental transparency settings.
- Mouse clickthrough
- Frameless window

## Known issues
- Does not work with fullscreen applications (Borderless Windowed is fine).
- May crash if a corrupt video file is selected.

## Developing
Topper is built with ElectronJS and should run on any supported operating system (Windows, *nix, Mac)

I recommend using *yarn*, but *npm* should work as well.

1. `yarn install`
2. `yarn start`

## Building
1. `yarn make`
2. Your output binary should be in the `out/make` directory.
