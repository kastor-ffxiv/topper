const { remote, ipcRenderer } = require('electron');
const FileType = require('file-type');
const readChunk = require('read-chunk');

// Handle receiving file info
ipcRenderer.on('openfile' , async function(event , data){
    console.log(data);
    if(data.canceled === true) {
        return;
    }

    getApplicableFileType(data.filePaths[0]).then(async (type) => {
        let evaluatedType = loadableFile(type);
        if(evaluatedType == null) {
            console.error('Invalid filetype :');
            console.error(type);
        }

        ipcRenderer.send('loadFile', { path: data.filePaths[0], type: evaluatedType});
    });
});

async function getApplicableFileType(filePath) {
    const buffer = readChunk.sync(filePath, 0, 4100);
    return await FileType.fromBuffer(buffer)
}

function loadableFile(fileType) {
    switch (fileType.ext) {
        case 'jpg':
        case 'png':
        case 'bmp':
        case 'gif':
        case 'webp':
            return 'image';
        case 'mp4':
        case 'webm':
        case 'mkv':
        case 'mov':
        case 'avi':
        case 'ogv':
            return 'video';
        case 'xml':
            return 'markup';
        default:
            return null;
    }
}