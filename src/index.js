const { app, BrowserWindow, Menu, dialog, ipcMain, Tray } = require('electron');
const path = require('path');
const sizeOf = require('image-size');
const prompt = require('electron-prompt');
let appTray, appMenu, mainWindow;
let clickthru_enabled = false;

const defaultWindowOptions = {
  title: 'Topper',
  icon: path.join(__dirname, '../res/icon.png'),
  width: 800,
  height: 600,
  alwaysOnTop: true,
  skipTaskbar: true,
  webPreferences: {
    preload: path.join(__dirname, 'preload.js')
  }
};
const transparencyMenu = {
  label: 'Transparency',
  submenu: [
    {
      label: '90%',
      click: async () => {
        mainWindow.setOpacity(0.9);
      }
    },
    {
      label: '80%',
      click: async () => {
        mainWindow.setOpacity(0.8);
      }
    },
    {
      label: '70%',
      click: async () => {
        mainWindow.setOpacity(0.7);
      }
    },
    {
      label: '60%',
      click: async () => {
        mainWindow.setOpacity(0.6);
      }
    },
    {
      label: '50%',
      click: async () => {
        mainWindow.setOpacity(0.5);
      }
    },
    {
      label: '40%',
      click: async () => {
        mainWindow.setOpacity(0.4);
      }
    },
    {
      label: '30%',
      click: async () => {
        mainWindow.setOpacity(0.3);
      }
    },
    {
      label: '20%',
      click: async () => {
        mainWindow.setOpacity(0.2);
      }
    },
    {
      label: '10%',
      click: async () => {
        mainWindow.setOpacity(0.1);
      }
    }
  ]
};

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

const createWindow = () => {
  bootstrapWindow();
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow();
  createTray();
  createMainMenu();
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.

ipcMain.on('loadFile', (event, data) => {
  // Replace the window with a frameless window
  let framelessOptions = defaultWindowOptions;
  framelessOptions.frame = false;

  let oldWindow = mainWindow;

  mainWindow = new BrowserWindow(framelessOptions);
  let oldPosition = oldWindow.getPosition();
  mainWindow.setPosition(oldPosition[0], oldPosition[1]);
  oldWindow.close();

  if(data.type === 'markup') {
    mainWindow.loadURL(data.path);
  } else {

    mainWindow.loadFile(data.path).then(() => {
      handleResize(data);
    }).catch((reason) => {
      if (reason.code === '') {
        // Nothing is actually wrong, electron just throws an error if we navigate while a file is loading, which loadFile
        // does by default... (See: https://github.com/electron/electron/issues/17526)
        handleResize(data);
      }
    });
  }
});

function loadUrl() {
  prompt({
    title: "Enter URL",
    label: "URL:",
    value: "https://example.org",
    inputAttrs: {
      type: 'url'
    },
    type: 'input',
    alwaysOnTop: true,
    height: 200
  }).then((data) => {
    if(data === null) {
      return;
    }

    if(!validUrl(data)) {
      console.error('Invalid URL:' + data);
    }

    let framelessOptions = defaultWindowOptions;
    framelessOptions.frame = false;

    let oldWindow = mainWindow;
    let oldPosition = oldWindow.getPosition();
    mainWindow = new BrowserWindow(framelessOptions);
    mainWindow.setPosition(oldPosition[0], oldPosition[1]);
    oldWindow.close();
    mainWindow.loadURL(data);
  })
}

function handleResize(data) {
  if(data.type === 'image') {
    // resize to image size
    sizeOf(data.path, async (err, dimensions) => {
      if(err) {
        console.error(err);
        return;
      }

      mainWindow.setSize(dimensions.width, dimensions.height);
    });
  } else if(data.type === 'video') {
    // getting cross platform video size sucks, i don't want to ship ffmpeg binaries or something
    // so just set to 720p and let them resize it if it sucks
    mainWindow.setSize(1280, 720);
  }

  // If it's html or something, let them do their own thing
}

function clickthru(toggle) {
  clickthru_enabled = toggle;
  mainWindow.setIgnoreMouseEvents(toggle);
}

function bootstrapWindow() {
  // Create the browser window.
  defaultWindowOptions.frame = true;
  mainWindow = new BrowserWindow(defaultWindowOptions);

  // and load the index.html of the app.
  mainWindow.loadFile(path.join(__dirname, 'index.html'));
}

function validUrl(string) {
  let url;
  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return !(url.protocol !== "http:" && url.protocol !== "https:");
}

function createTray() {
  appTray = new Tray(path.join(__dirname, '../res/icon.png'));
  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Toggle Clickthru',
      click: async () => {
        clickthru(!clickthru_enabled);
      }
    },
    {
      label: 'Change File',
      role: 'open',
      click: async () => {
        dialog.showOpenDialog({properties: ['openFile']}).then((openFile) => {
          mainWindow.webContents.send('openfile', openFile);
        });
      }
    },
    {
      label: 'Load URL',
      click: () => {
        loadUrl();
      }
    },
    transparencyMenu,
    {
      label: 'Reset Window',
      click: async () => {
        // Replace frameless window with default window
        let oldWindow = mainWindow;
        bootstrapWindow();
        oldWindow.close();
      }
    },
    {
      type: 'separator'
    },
    {
      role: 'close',
      click: async () => {
        app.quit();
      }
    },
  ]);
  appTray.setToolTip('Topper');
  appTray.setContextMenu(contextMenu);
}

function createMainMenu() {
  let mainMenuTemplate = [
    {
      label: 'File',
      submenu: [
        { role: 'close' },
        { type: 'separator' },
        {
          role: 'open',
          label: 'Load File',
          click: async () => {
            dialog.showOpenDialog({properties: ['openFile']}).then((openFile) => {
              mainWindow.webContents.send('openfile', openFile);
            });
          }
        },
        {
          label: 'Load URL',
          click: () => {
            loadUrl();
          }
        },
        {
          label: 'Toggle Clickthru',
          click: async () => {
            clickthru(!clickthru_enabled);
          }
        },
        transparencyMenu
      ]
    },
    {
      role: 'help',
      submenu: [
        {
          label: 'About/Source Code',
          click: async () => {
            const { shell } = require('electron');
            await shell.openExternal('https://gitlab.com/kastor-ffxiv/topper');
          }
        }
      ]
    }
  ];

  appMenu = Menu.buildFromTemplate(mainMenuTemplate);
  Menu.setApplicationMenu(appMenu);
}